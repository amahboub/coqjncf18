From mathcomp Require Import all_ssreflect all_fingroup all_algebra.


(*** Basics: data structures and logic ***)

(* A Sandbox name space *)
Module Sandbox.

(* Type bool and its inhabitants: *)
Print bool.

(* Definition of boolean negation by pattern-matching: *)
Definition negb (b : bool) : bool :=
  if b then false else true.

(* What you see is what you get: *)
Check (negb true).

(* Computation: *)
Eval compute in (negb true).

(* Proof by computation, using the conversion rule: *)
Lemma negbT : negb true = false. by []. Show Proof. Qed.

(* Proof by case analysis: *)
Lemma negbK b : negb (negb b) = b.
Proof.
case: b.
  by [].
by [].
Qed.

(* Justification of the proof by case analysis: *)
About bool_ind.

(* Type nat and its inhabitants: *)
Print nat.

(* We can use notations 0 for O (zero) and _.+1 for successor (S): *)

(* Definition by pattern-matching: *)
Definition nat_eq0 (n : nat) : bool :=
  if n is S k then false else true.

(* Proof by computation: *)
Lemma nat_seq0S (n : nat) : nat_eq0 n.+1 = false.
Proof. by []. Qed.

(* Definition by recurrence: *)
Fixpoint addn (n m : nat) : nat :=
  match n with
  |0 => m
  |k.+1 => (addn k m).+1
  end.

(* Proof by computation: *)
Lemma addSnm n m : addn n.+1 m = (addn n m).+1.
Proof. by []. Qed.

(* Proof by recurrence: *)
Lemma addnSm n m : addn n m.+1 = (addn n m).+1.
Proof.
elim: n.
  by [].
move=> n ihn /=.
rewrite ihn.
by [].
Qed.

(* Definition by recurrence: *)
Fixpoint eqn (n m : nat) : bool :=
  match n, m with
  |0, 0 => true
  |S k, S l => eqn k l
  |_, _ => false
  end.

(* Justification of the proof and definition by recurrence: *)
About nat_ind.

(* What is the difference between (eqn k l) and (k = l) ? *)
About eq.


(*** Inference of implicit information ***)

(* A polymorphic type for lists (in fact already in the prelude): *)
Inductive list (A : Type) := nil | cons : A -> list A -> list A.

Fail Check (cons 3 (cons 4 nil)).
Check (cons nat 3 (cons nat 4 (nil nat))).

(* Avoid verbose duplications of parameters *)
Arguments cons {A}.
Arguments nil {A}.
About nil.
About cons.

Check (cons 3 (cons 4 nil)).
Fail Check (cons nat 3 (cons nat 4 (nil nat))).

End Sandbox.


(* Unification/matching unfolds definitions and perform computation, so that we
do not need to over-specify the theorems to be applied *)
Definition zero : nat := 0.

Lemma foo (a b : nat) : a + (zero + 2 * zero) = a.
Proof.
by  rewrite addn0.
Qed.

(* Example: we can define types which pair a data type with a specification. *)
Structure ntuple (n : nat) := {val : seq nat ; valP : size val = n}.

(* Groups, and other structures are generalization of the latter example *)

Open Scope group_scope.
(* Unification is used here to infer the assumptions: it finds out
   that the intersection of a group and of the normalizer of a group
   has a canonical group structure. *)
Lemma bar (gT : finGroupType) (H K : {group gT}) : 0 < #|'N(K) :&: H|.
Proof.
have h := cardG_gt0.
   Set Printing Coercions. Unset Printing Coercions.
apply: h.
Qed.

Close Scope group_scope.

(*The type of matrices depend on two natural numbers,
specifying the dimension of the matrix. *)

(*Product operation on the type of matrices: it enforces a ring structure
on the coefficients, and compatible sizes for the two arguments. *)
About mulmx.

(* A datastructure for integers: two copies of nat *)
Print int.


Variable A : 'M[int]_(3,4).

(* The proof assistant knows that type int is equipped with a ring structure:*)

Check mulmx (trmx A) A.




(*** Computations and Oracles ****)

(* Pebble arithmetics is very limited: *)
Fail Eval compute in 1000 * 1000.


(* A binary representation does much better: *)
From Coq Require Import ZArith.

Open Scope Z_scope.

(* Z has two copies of words of bits with no trailing zeros: *)
Print Z.

Print positive.

Definition c : Z :=
  90377629292003121684002147101760858109247336549001090677693.


Definition f1 := 588120598053661.
Definition f2 := 260938498861057.
Definition f3 := 760926063870977.
Definition f4 := 773951836515617.

Lemma factors_c : c = f1 * f2 * f3 * f4. Proof. Time reflexivity. Qed.


Fixpoint fact_loop (accum p : positive) (fuel : nat) :=
  match fuel with
  |0%nat => accum
  |1%nat => accum
  |f.+1 =>  fact_loop (p * accum)%positive (p - 1) f
  end.

Definition fact p := fact_loop 1 p (nat_of_pos p).

Time Eval compute in fact (1000)%positive.

(* The proof assistant has several strategies to evaluate programs inside
the logic: vm_compute (for Virtual Machine) is way more optimized: *)
Time Eval vm_compute in fact (1000)%positive.

(* Reflection based tactics: a toy procedure to normalize
expressions featuring a binary operation op2 and two constants z and u. *)


Variables (dom : Type) (op2 : dom -> dom -> dom) (z u : dom).

(* We assume op2 x z = op2 z x = x for any x *)
Hypothesis (op2zd : left_id z op2).
Hypothesis (op2dz : right_id z op2).

(* A useful auxiliary data structure: abstract syntax trees *)
Inductive expr :=
  Zero | One | Op2 : expr -> expr -> expr.

(* Evaluation of an expr to a term of type dom: *)
Fixpoint eval_expr (e : expr) : dom :=
  match e with
  |Zero => z
  |One => u
  |Op2 e1 e2 => op2 (eval_expr e1) (eval_expr e2)
  end.

(* Normalization, i.e. ereasing occurrences of z *)
Fixpoint normal_expr (e : expr) : expr :=
  match e with
  |Zero => e
  |One => e
  |Op2 e1 e2 =>
   let ne1 := normal_expr e1 in
   if ne1 is Zero then normal_expr e2
   else
     let ne2 := normal_expr e2 in
     if ne2 is Zero then ne1 else Op2 ne1 ne2
  end.

(* Correctness theorem, relies on the assumptions on op2 and z and u *)
Lemma normal_correct (e1 e2 : expr) :
  normal_expr e1 = normal_expr e2 ->
  eval_expr e1 = eval_expr e2.
Proof.
Admitted.

(* If an oracle guesses e1 and e2, we can prove this identity "by computation" *)
Lemma baz : op2 z (op2 u z) = u.
Proof.
pose e1 := Op2 Zero (Op2 One Zero).
pose e2 := One.
apply: (normal_correct e1 e2). by [].
Qed.

(* The latter example is a baby ring tactic. Here is the real one: *)
Open Scope nat_scope.

Lemma ring_test (f :int -> bool) (b : bool) (n m : nat) (k : int):
  (if (f k) then n else m) + 2 * m  + 0 * n =
  m + (if (f k) then n else m) + m.
Proof.
  ring.
Qed.


(* Interval arithmetic based estimation of definite integrals *)
From Coq Require Import Reals.

From Interval Require Import Interval_tactic.

Open Scope R_scope.

Lemma ex_int1 :
  Rabs (RInt (fun x => /(1 + x*x)) 0 1 - PI/4) <= 1/1000000000000000000.
Proof.
Time interval with
  (i_integral_width (-60), i_integral_deg 15, i_integral_depth 3, i_prec 70).
Qed.

Lemma ex_int2 : (RInt_gen (fun tau => (1 + (1/2 * ln (1 + 9/4/tau^2) + 41396/10000 + ln PI) / ln tau)^2 / (1 + 1/4 / tau^2) *
  (powerRZ tau (-2) * (ln tau)^2)) (at_point 100000) (Rbar_locally p_infty) - 317742 / 100000000) <= 1 / 100000000.
Time interval with
  (i_integral_width (-26), i_integral_deg 15, i_integral_depth 18, i_prec 30).
Qed.