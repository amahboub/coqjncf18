# coqjncf18

Support utilisé dans un cours de preuves formelles donné aux [Journées Nationales du Calcul Formel 2018](https://jncf2018.lip6.fr/).

Il épend des bibliothèques externes Mathematical Components et Coq.Interval, cette dernière pour le dernier exemple du cours seulement. 
Coq.Interval dépend à son tour des bibliothèques Coquelicot et Flocq.

Testé avec Coq 8.5 à 8.8, Flocq 2.5, MathComp 1.6, Coquelicot 3.0.